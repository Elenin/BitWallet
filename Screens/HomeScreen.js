import React from 'react';

import {
  AppRegistry,
  Button, View, StyleSheet
} from 'react-native';

import { StackNavigator } from 'react-navigation';

import ScanScreen from './ScanScreen';
import BlaScreen from './BlaScreen';
import PickFileScreen from './PickFile';


class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Welcome',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
  <View style={styles.container}>
     <View style={styles.buttonContainer}>
      <Button
        title="Go to Bla"
        onPress={() =>
          navigate('BlaScreen', { name: 'Bla' })
        }
      />  
    </View>
    <View style={styles.buttonContainer}>
      <Button
        title="Go to Scan"
        onPress={() =>
          navigate('ScanScreen', { name: 'Scan' })
        }
      />  
    </View>
    <View style={styles.buttonContainer}>
      <Button
        title="PickFile"
        onPress={() =>
          navigate('PickFileScreen', { name: 'Pick file' })
        }
      />  
    </View>
  </View>
    );
  }
}

export default HS = StackNavigator({
  Home: { screen: HomeScreen },
  ScanScreen: {screen: ScanScreen },
  BlaScreen: {screen: BlaScreen },
  PickFileScreen: {screen: PickFileScreen },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    flex: 1,
  }
});
