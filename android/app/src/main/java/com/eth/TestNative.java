package com.eth;

import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import org.ethereum.geth.*;

import android.util.*;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TestNative extends ReactContextBaseJavaModule {
    public TestNative(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "TestNative";
    }

    @ReactMethod
    public void addKey(String path, String pass1, String pass2, Callback cb) {
        File f = new File(path);
        boolean rprm = f.canRead();
        if(rprm) {
            int size = (int) f.length();
            byte[] bytes = new byte[size];
            try {
                BufferedInputStream buf = new BufferedInputStream(new FileInputStream(f));
                buf.read(bytes, 0, bytes.length);
                buf.close();

                NodeHolder nh = NodeHolder.getInstance();
                KeyStore ks = nh.getKs();

                Account a = ks.importKey(bytes, pass1, pass2);

                cb.invoke("Success! Imported account: " + a.getAddress().getHex());
            } catch (Exception e) {
                e.printStackTrace();
                cb.invoke("Error: " + e.getLocalizedMessage());
            }
        }
    }

    @ReactMethod
    public void test(String message, Callback cb) {
        try {
            NodeHolder nh = NodeHolder.getInstance();
            Node node = nh.getNode();
            Context ctx = new Context();
            if (node != null) {
                NodeInfo info = node.getNodeInfo();
                PeerInfos pi = node.getPeersInfo();
                long peerssz = pi.size();
                if(peerssz > 0) {
                   String ra = pi.get(0).getRemoteAddress();
                    String rid = pi.get(0).getID();
                    android.util.Log.d("_REMOTE_PEER", "PEER: " + ra + " " + rid);
                }

                EthereumClient ethereumClient = node.getEthereumClient();


//                BigInt value = Geth.newBigInt(1);
//                BigInt gasLimit = Geth.newBigInt(50000);
//                BigInt gasPrice = ethereumClient.suggestGasPrice(ctx);
//                String testData = "Hello World";
//                byte[] data = testData.getBytes();
                Account newAcc = nh.getAcc();

                BigInt balanceAt = ethereumClient.getBalanceAt(ctx, newAcc.getAddress(), -1);
                android.util.Log.d("_ETHER BALANCE", "*** ETHER BALANCE 1 " + balanceAt.toString() + " " + newAcc.getAddress().getHex());
                balanceAt = ethereumClient.getBalanceAt(ctx, new Address("0x753e05802ec3e222a1dd4520536059cfe50e50b2"), -1);
                cb.invoke("*** ETHER BALANCE " + balanceAt.toString() + " " + newAcc.getAddress().getHex());
                android.util.Log.d("_ETHER BALANCE", "*** ETHER BALANCE 2 " + balanceAt.toString() + " 0x753e05802ec3e222a1dd4520536059cfe50e50b2");


//                long nonce = ethereumClient.getPendingNonceAt(ctx, newAcc.getAddress()); // 1. Blocks the Thread if used? Syncing maybe?

//                Transaction transaction = Geth.newTransaction(nonce, new Address("753e05802ec3e222a1dd4520536059cfe50e50b2"), value, gasLimit, gasPrice, data);

//                nh.getKs().timedUnlock(newAcc, "!3351aa99", 10000000);
//                transaction = nh.getKs().signTx(newAcc, transaction, null); // 2. Why BigInt needed?

//                android.util.Log.d("", "Cost: " + transaction.getCost());
//                android.util.Log.d("", "GasPrice: " + transaction.getGasPrice());
//                android.util.Log.d("", "Gas: " + transaction.getGas());
//                android.util.Log.d("", "Nonce: " + transaction.getNonce());
//                android.util.Log.d("", "Value: " + transaction.getValue());
//                android.util.Log.d("", "Sig-Hash Hex: " + transaction.getSigHash().getHex());
//                android.util.Log.d("", "Hash Hex: " + transaction.getHash().getHex());
//                android.util.Log.d("", "Data-Length: " + transaction.getData().length);
//                android.util.Log.d("", "To: " + transaction.getTo().getHex());
//                android.util.Log.d("", "Sender: " + transaction.getFrom(new BigInt(0)).getHex());

//                ethereumClient.sendTransaction(ctx, transaction);

                return;
            }
            cb.invoke("node was null");
        } catch (Exception e) {
            android.util.Log.d("", e.getMessage());
            e.printStackTrace();
        }
    }
}